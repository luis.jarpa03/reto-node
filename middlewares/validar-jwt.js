const jwt = require('jsonwebtoken');

const validarJWT = (req, res, next) => {

    // Leer token
    const token = req.header('x-token');

    if (!token) {
        return res.status(401).json({
            ok: false,
            msg: 'No hay token en la petición'
        });
    }

    try {

        const {
            uid
        } = jwt.verify(token, process.env.JWT_KEY);
        req.uid = uid;

        next();

    } catch (error) {
        return res.status(401).json({
            ok: false,
            msg: 'Token no válido'
        })
    }
}



const validarYearAndTitle = (req, res, next) => {

    // Leer Año
    const title = req.params.title;
    //const title = req.header('title');
    const year = req.header('year');

    try {

        if (year) {

            const IsNumber = Number.isInteger(Number(year));

            if (!IsNumber) {
                return res.status(401).json({
                    ok: false,
                    msg: 'Year debe ser numérico AAAA'
                });
            }
        }

        if (!title) {
            return res.status(401).json({
                ok: false,
                msg: 'Debe ingresar un title para realizar la solicitud'
            });
        }
        next();

    } catch (error) {
        return res.status(401).json({
            ok: false,
            msg: 'Year debe ser Numérico'
        })
    }

}

const validarYear = (req, res, next) => {

    // Leer Año

    const year = req.header('year');

    try {

        if (year) {

            const IsNumber = Number.isInteger(Number(year));

            if (!IsNumber) {
                return res.status(401).json({
                    ok: false,
                    msg: 'Year debe ser numérico AAAA'
                });
            }
        }

        next();

    } catch (error) {
        return res.status(401).json({
            ok: false,
            msg: 'Year debe ser Numérico'
        })
    }

}

module.exports = {
    validarJWT,
    validarYear,
    validarYearAndTitle
}