const {
    Schema,
    model
} = require('mongoose');

const PeliculaSchema = Schema({

    Title: {
        type: String,
        require: true

    },
    Year: {
        type: String,
        require: true

    },
    Genre: {
        type: String,
        require: true
    },
    Director: {
        type: String,
        require: true
    },
    Actors: {
        type: String,
        require: true
    },
    Plot: {
        type: String,
        require: true
    },
    Ratings: {
        type: [{
            Source: {
                type: String,
                require: true
            },
            Value: {
                type: String,
                require: true
            }

        }],
        require: true
    }
}, {

    timestamps: true
});

PeliculaSchema.method('toJSON', function () {

    const {
        _id,
        ...object
    } = this.toObject();
    return object;

});

module.exports = model('Peliculas', PeliculaSchema);