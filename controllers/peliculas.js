const {
    response
} = require("express");
const axios = require('axios');

const
    Request = require("request");

const Peliculas = require('../models/peliculas');

async function getPelicula(uri) {
    try {
        const response = await axios.get(uri);
        const data = JSON.stringify(response.data);
        return data;
    } catch (error) {
        return false;
    }
}

const buscarPeliculas = async (req, res = response) => {

    const _title = req.params.title;
    console.log(_title);

    const year = req.header('year');
    let uri = 'http://www.omdbapi.com/?apikey=' + process.env.API_KEY +
        '&r=json&t=' + _title;

    if (year) {
        uri = 'http://www.omdbapi.com/?apikey=' + process.env.API_KEY +
            '&r=json&t=' + _title + '&y=' + Number(year);
    }


    const search = await getPelicula(uri);
    const data = JSON.parse(search);
    try {

        const pelicula = new Peliculas(data);
        const existePelicula = await Peliculas.findOne({
            Title: pelicula.Title
        });
        if (!existePelicula) {
            await pelicula.save();
        }

    } catch (error) {
        console.log(error);
    }

    const Just5 = await Peliculas.find({
        Title: {
            $ne: null
        }
    }).sort({
        createdAt: 'desc'
    });


    try {
        res.json({
            ok: true,
            registros: Just5.length,
            peliculas: Just5

        });
    } catch (error) {
        res.json({
            ok: false,
            registros: 0,
            peliculas: error
        });
    }
}


const obtenerPeliculas = async (req, res = response) => {


    const pagina = req.header('page');
    const _pageNumber = Number(pagina) || 1;
    const _pageSize = 5;

    const Just5 = await Peliculas.find({})
        .sort({
            createdAt: 'desc'
        })
        .skip(_pageNumber > 0 ? ((_pageNumber - 1) * _pageSize) : 0)
        .limit(_pageSize)

    try {
        res.json({
            ok: true,
            PaginaActual: _pageNumber,
            RegistrosPorPagina: _pageSize,
            CantidadRegistros: Just5.length,
            Peliculas: Just5
        });

    } catch (error) {
        res.json({
            ok: false,
            PaginaActual: _pageNumber,
            RegistrosPorPagina: 5,
            CantidadRegistros: 0,
            Peliculas: error
        });
    }
}


const searchAndReplace = async (req, res = response) => {


    const {
        movie,
        find,
        replace
    } = req.body;

    const searchRegExp = new RegExp(find, 'g');
    const _peliculas = await Peliculas.find({

            "Title": {
                "$regex": movie,
                "$options": "i"
            }
        }).select("Plot")
        .sort({
            createdAt: 'desc'
        });

    _peliculas.forEach(function (key) {
        key.Plot = key.Plot.replace(searchRegExp, replace);
    });

    try {
        res.json({
            ok: true,
            CantidadRegistros: _peliculas.length,
            Resultado: _peliculas
        });

    } catch (error) {
        res.json({
            ok: false,
            CantidadRegistros: 0,
            Resultado: error
        });
    }
}


module.exports = {
    buscarPeliculas,
    obtenerPeliculas,
    searchAndReplace
}